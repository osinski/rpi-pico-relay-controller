#include <string.h>
#include <stdlib.h>

#include <zephyr/kernel.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/drivers/uart.h>
#include <zephyr/usb/usb_device.h>

#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(main, CONFIG_APP_LOG_LEVEL);

BUILD_ASSERT(DT_NODE_HAS_COMPAT(DT_CHOSEN(zephyr_console), zephyr_cdc_acm_uart),
	    "Console device is not ACM CDC UART device");


static const struct gpio_dt_spec led = GPIO_DT_SPEC_GET(DT_NODELABEL(led0), gpios);
static const struct gpio_dt_spec relays_specs[] = {
	GPIO_DT_SPEC_GET(DT_NODELABEL(relay1), gpios),
	GPIO_DT_SPEC_GET(DT_NODELABEL(relay2), gpios),
	GPIO_DT_SPEC_GET(DT_NODELABEL(relay3), gpios),
	GPIO_DT_SPEC_GET(DT_NODELABEL(relay4), gpios),
};
const struct device *usb_device = DEVICE_DT_GET(DT_CHOSEN(zephyr_console));

K_SEM_DEFINE(recv_semaphore, 0, 1);

#define RECV_MSG_LEN 16
static uint8_t recv_buf[RECV_MSG_LEN] = {};
static size_t recv_buf_pos = 0;

static void wait_for_console_connection(void);
static bool config_led(void);
static bool config_relays_gpios(void);
static void handle_recvd_string(void);
void uart_rx_isr(const struct device* dev, void* user_data);

int main(void)
{
	if (usb_enable(NULL) != 0) {
		return 0;
	}
	
	wait_for_console_connection();

	if (config_led() == false) {
		return 0;
	}

	if (config_relays_gpios() == false) {
		return 0;
	}

	uart_irq_callback_set(usb_device, uart_rx_isr);
	uart_irq_rx_enable(usb_device);

	while (1) {
		if (k_sem_take(&recv_semaphore, K_FOREVER) == 0) {
			handle_recvd_string();
		}
	}

	return 0;
}

static void wait_for_console_connection(void)
{
	uint32_t dtr = 0;
	while (!dtr) {
		uart_line_ctrl_get(usb_device, UART_LINE_CTRL_DTR, &dtr);
		k_sleep(K_MSEC(100));
	}
}

static bool config_led(void)
{
	if (gpio_is_ready_dt(&led) == false) {
		return false;
	}
	
	int ret = gpio_pin_configure_dt(&led, GPIO_OUTPUT_ACTIVE);
	if (ret < 0) {
		return false;
	}

	return true;
}

static bool config_relays_gpios(void)
{
	for (size_t i = 0; i < ARRAY_SIZE(relays_specs); i++) {
		if (gpio_is_ready_dt(&relays_specs[i]) == false) {
			return false;
		}

		int ret = gpio_pin_configure_dt(&relays_specs[i], GPIO_OUTPUT_INACTIVE);
		if (ret < 0) {
			return false;
		}
	}

	return true;
}

static void handle_recvd_string(void)
{
	size_t relay_idx = 0;
	int new_state = 0;
	bool update_relay_state = true;
	
	char* rest = NULL;
	char* token = strtok_r(recv_buf, " ", &rest);
    char msg_part[32] = {};
	size_t token_idx = 0;
	while (token != NULL) {
		if (token_idx == 0) {
			if (strncmp(token, "wlacz", 5) == 0) {
				new_state = 1;
				strcpy(msg_part, "OK, wlaczam przekaznik nr");
			} else if (strncmp(token, "wylacz", 6) == 0) {
				new_state = 0;
				strcpy(msg_part, "OK, wylaczam przekaznik nr");
			} else if (strncmp(token, "status", 6) == 0) {
				printk("OK, stan przekaznikow: %d,%d,%d,%d\n",
					gpio_pin_get_dt(&relays_specs[0]),
					gpio_pin_get_dt(&relays_specs[1]),
					gpio_pin_get_dt(&relays_specs[2]),
					gpio_pin_get_dt(&relays_specs[3]));
				update_relay_state = false;
				break;
			} else {
				update_relay_state = false;
				printk("NOK, nierozpoznana komenda\n");
				break;
			}
		} else if (token_idx == 1) {
			char* endptr;
			relay_idx = strtol(token, &endptr, 10);
			if (*endptr == '\0') {
				if (relay_idx > 0 && relay_idx <= ARRAY_SIZE(relays_specs)) {
					printk("%s %d\n", msg_part, relay_idx);
					relay_idx--;
				} else {
					update_relay_state = false;
					printk("NOK, niepoprawny nr przekaznika\n");
				}
			} else {
				update_relay_state = false;
				printk("NOK, niepoprawny nr przekaznika\n");
			}
		} else {
			update_relay_state = false;
			printk("NOK, komenda zbyt dluga\n");
			break;
		}

		token_idx++;
		token = strtok_r(NULL, " ", &rest);
	}

	if (update_relay_state) {
		gpio_pin_set_dt(&relays_specs[relay_idx], new_state);
	}
}

void uart_rx_isr(const struct device* dev, void* user_data)
{
	ARG_UNUSED(user_data);

	if (!uart_irq_update(dev)) {
		return;
	}

	if (!uart_irq_rx_ready(dev)) {
		return;
	}

	char c;
	while (uart_fifo_read(dev, &c, 1) > 0) {
		if ((c == '\n' || c == '\r') && recv_buf_pos > 0) {
			recv_buf[recv_buf_pos] = '\0';
			recv_buf_pos = 0;
			k_sem_give(&recv_semaphore);
		} else if (recv_buf_pos < (sizeof(recv_buf) - 1)) {
			recv_buf[recv_buf_pos++] = c;
		}
	}
}

