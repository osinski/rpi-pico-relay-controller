# Raspberry Pi Pico Relay Controller.

## Software

This repo is based on
[zephyr example application](https://github.com/zephyrproject-rtos/example-application)

**Make sure you have zephyr installed** 
(see [getting started guide](https://docs.zephyrproject.org/latest/develop/getting_started/index.html))


### Initialization

The first step is to initialize the workspace folder (e.g.
``workspace-rpi-pico-relay-controller``) where the ``rpi-pico-relay-controller`` and all
Zephyr modules will be cloned. To do this:

* init workspace with:
```shell
west init -m https://gitlab.com/osinski/rpi-pico-relay-controller --mr master workspace-rpi-pico-relay-controller
```
or (if you want to authenticate with ssh)
```shell
west init -m git@gitlab.com:osinski/rpi-pico-relay-controller --mr master workspace-rpi-pico-relay-controller
```
* update Zephyr modules:
```shell
cd workspace-rpi-pico-relay-controller
west update
```


### Building

To build the application, run the following command:

```shell
west build -p auto  ./rpi-pico-relay-controller/app -d build -b rpi_pico -- -DBOARD_FLASH_RUNNER=jlink
```

The ``-DBOARD_FLASH_RUNNER=jlink`` is needed if you intent to use JLink as flashing/debugging tool.
See following documentation pages for more info:
* [Building, Flashing and Debugging](https://docs.zephyrproject.org/latest/develop/west/build-flash-debug.html#flash-and-debug-runners)
* [Debug Probes](https://docs.zephyrproject.org/latest/develop/flash_debug/probes.html)


### Flashing

Use

```shell
west flash
```

to flash the board (via JLink)


## Hardware

This project uses RPi Pico board (both normal and wireless versions could be used, but wireless
is not needed in this project) and additional [relay board](https://kamami.pl/en/other-modules/1178283-pico-relay-board-4-channel-module-with-relays-for-raspberry-pi-pico-21178.html).

